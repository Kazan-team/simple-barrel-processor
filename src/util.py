# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information

from nmigen import Signal
from typing import Union, Tuple, List, Optional, Any
from nmigen.hdl.rec import Layout, Record, Direction
from nmigen.hdl.ast import Assign
from typing_extensions import Protocol
import enum

SignalInputShape = Union[int, Tuple[int, bool]]
# workaround for recursive types not being supported yet in mypy
# see https://github.com/python/mypy/issues/731
_LayoutInput3 = Union[Layout, List[Any]]
_LayoutInputField3 = Union[Tuple[str, SignalInputShape, Direction],
                           Tuple[str, Union[SignalInputShape, _LayoutInput3]]]
_LayoutInput2 = Union[Layout, List[_LayoutInputField3]]
_LayoutInputField2 = Union[Tuple[str, SignalInputShape, Direction],
                           Tuple[str, Union[SignalInputShape, _LayoutInput2]]]
_LayoutInput1 = Union[Layout, List[_LayoutInputField2]]
_LayoutInputField1 = Union[Tuple[str, SignalInputShape, Direction],
                           Tuple[str, Union[SignalInputShape, _LayoutInput1]]]
LayoutInput = Union[Layout, List[_LayoutInputField1]]
LayoutInputField = Union[Tuple[str, SignalInputShape, Direction],
                         Tuple[str, Union[SignalInputShape, LayoutInput]]]


def layout_assert_direction(layout: Union[Layout, Record],
                            expected_direction: Direction) -> None:
    if isinstance(layout, Record):
        layout = layout.layout
    for name, shape, direction in layout:
        if isinstance(shape, Layout):
            layout_assert_direction(shape, expected_direction)
        else:
            assert direction == expected_direction


def layout_assert_directed(layout: Union[Layout, Record]) -> None:
    if isinstance(layout, Record):
        layout = layout.layout
    for name, shape, direction in layout:
        if isinstance(shape, Layout):
            layout_assert_directed(shape)
        else:
            assert direction != Direction.NONE


def layout_reverse(layout: Union[Layout, Record]) -> Layout:
    if isinstance(layout, Record):
        layout = layout.layout
    fields: List[LayoutInputField] = []
    for name, shape, direction in layout:
        if direction == Direction.FANOUT:
            direction = Direction.FANIN
        elif direction == Direction.FANIN:
            direction = Direction.FANOUT
        else:
            assert direction == Direction.NONE
        if isinstance(shape, Layout):
            assert direction == Direction.NONE
            shape = layout_reverse(shape)
            fields.append((name, shape))
        else:
            fields.append((name, shape, direction))
    return Layout(fields)


def layout_apply_default_direction(layout: Union[Layout, Record],
                                   default_direction: Direction) -> Layout:
    if isinstance(layout, Record):
        layout = layout.layout
    fields: List[LayoutInputField] = []
    for name, shape, direction in layout:
        if direction == Direction.FANOUT or direction == Direction.FANIN:
            pass
        else:
            assert direction == Direction.NONE
            if not isinstance(shape, Layout):
                direction = default_direction
        if isinstance(shape, Layout):
            assert direction == Direction.NONE
            shape = layout_apply_default_direction(shape, default_direction)
            fields.append((name, shape))
        else:
            fields.append((name, shape, direction))
    return Layout(fields)


class VisitRecordsCallback(Protocol):
    def __call__(self, name: str, *signals: Tuple[Signal, Direction]) -> None:
        ...


def visit_records(visitor: VisitRecordsCallback,
                  records: Union[List[Record], Record],
                  layouts: Optional[Union[List[Layout],
                                          Record]] = None) -> None:
    if isinstance(records, Record):
        records = [records]
    for record in records:
        assert isinstance(record, Record)
    if layouts is None:
        layouts = [record.layout for record in records]
    elif isinstance(layouts, Layout):
        layouts = [layouts]
    assert len(records) == len(layouts)
    if len(layouts) == 0:
        return
    first_layout = layouts[0]
    for layout in layouts:
        assert isinstance(layout, Layout)
        for name in layout.fields:
            assert name in first_layout.fields
    for name, first_shape, first_direction in first_layout:
        if isinstance(first_shape, Layout):
            assert first_direction == Direction.NONE
            arg_records = []
            arg_layouts = []
            for layout, record in zip(iter(layouts), iter(records)):
                shape, direction = layout[name]
                assert isinstance(shape, Layout)
                assert direction == Direction.NONE
                arg_layouts.append(shape)
                arg_record = record[name]
                assert isinstance(arg_record, Record)
                arg_records.append(arg_record)
            visit_records(visitor, arg_records, arg_layouts)
        else:
            signals = []
            for layout, record in zip(iter(layouts), iter(records)):
                shape, direction = layout[name]
                assert isinstance(shape, (int, tuple))
                signal = record[name]
                assert isinstance(signal, Signal)
                signals.append((signal, direction))
            visitor(name, *signals)


class RecordConnectDirection(enum.Enum):
    InToOut = enum.auto()
    OutToIn = enum.auto()


AssignList = List[Assign]


def connect_records(a: Record,
                    b: Record,
                    direction: RecordConnectDirection,
                    a_layout: Optional[Layout] = None,
                    b_layout: Optional[Layout] = None,
                    assignments: Optional[AssignList] = None) -> AssignList:
    if assignments is None:
        assignments = []
    if a_layout is None:
        a_layout = a.layout
    if b_layout is None:
        b_layout = b.layout

    def visitor(name: str,
                a_sd: Tuple[Signal, Direction],
                b_sd: Tuple[Signal, Direction]) -> None:
        a_signal, a_direction = a_sd
        b_signal, b_direction = b_sd
        if a_direction == Direction.FANIN:
            assert b_direction == Direction.FANOUT
            is_a_to_b = False
        else:
            assert a_direction == Direction.FANOUT
            assert b_direction == Direction.FANIN
            is_a_to_b = True
        if direction == RecordConnectDirection.InToOut:
            is_a_to_b = not is_a_to_b
        else:
            assert direction == RecordConnectDirection.OutToIn
        if is_a_to_b:
            assignments.append(b_signal.eq(a_signal))
        else:
            assignments.append(a_signal.eq(b_signal))

    visit_records(visitor, [a, b], [a_layout, b_layout])
    return assignments
