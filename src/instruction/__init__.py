#!/usr/bin/env python3
# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information
from nmigen import Signal
from .format import (Format,
                     RType as _RType,
                     IType as _IType,
                     SType as _SType,
                     BType as _BType,
                     UType as _UType)
from ..config import Config


class Instruction(Signal):
    def __init__(self, config: Config):
        Signal.__init__(self, config.ilen)
        self.r_type = _RType(self, config)
        self.i_type = _IType(self, config)
        self.s_type = _SType(self, config)
        self.b_type = _BType(self, config)
        self.u_type = _UType(self, config)
