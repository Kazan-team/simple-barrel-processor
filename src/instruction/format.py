#!/usr/bin/env python3
# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information
from ..config import Config
from nmigen import Cat, Repl, C, Value
from nmigen.tools import log2_int
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from . import Instruction


class _Fields:
    opcode5: Value
    opcode7: Value
    rd: Value
    funct3: Value
    rs1: Value
    rs2: Value
    funct7: Value
    shamt: Value
    i_type_imm12: Value
    imm_sign_bit: Value
    s_type_imm7: Value
    s_type_imm5: Value
    b_type_imm7: Value
    b_type_imm5: Value
    u_type_imm20: Value
    i_type_imm: Value
    s_type_imm: Value
    b_type_imm: Value
    u_type_imm: Value

    def __init__(self, instruction: 'Instruction', config: Config) -> None:
        self.opcode5 = instruction.part(2, 5)
        self.opcode7 = instruction.part(0, 7)
        self.rd = instruction.part(7, 5)
        self.funct3 = instruction.part(12, 3)
        self.rs1 = instruction.part(15, 5)
        self.rs2 = instruction.part(20, 5)
        self.funct7 = instruction.part(25, 7)
        self.shamt = instruction.part(20, log2_int(config.xlen))
        self.i_type_imm12 = instruction.part(20, 12)
        self.imm_sign_bit = instruction[31]
        self.s_type_imm7 = instruction.part(25, 7)
        self.s_type_imm5 = instruction.part(7, 5)
        self.b_type_imm7 = instruction.part(25, 7)
        self.b_type_imm5 = instruction.part(7, 5)
        self.u_type_imm20 = instruction.part(12, 20)
        self.i_type_imm = Cat(self.i_type_imm12,
                              Repl(self.imm_sign_bit, config.xlen - 12))
        self.s_type_imm = Cat(self.s_type_imm5,
                              self.s_type_imm7,
                              Repl(self.imm_sign_bit, config.xlen - 12))
        self.b_type_imm = Cat(C(0, 1),
                              self.b_type_imm5.part(1, 4),
                              self.b_type_imm7,
                              self.b_type_imm5[0],
                              Repl(self.imm_sign_bit, config.xlen - 12 - 1))
        self.u_type_imm = Cat(C(0, 12),
                              self.u_type_imm20,
                              Repl(self.imm_sign_bit, config.xlen - 32))


class Format:
    pass


class RType(Format):
    def __init__(self, instruction: 'Instruction', config: Config) -> None:
        fields = _Fields(instruction, config)
        self.opcode7 = fields.opcode7
        self.opcode5 = fields.opcode5
        self.rd = fields.rd
        self.funct3 = fields.funct3
        self.rs1 = fields.rs1
        self.rs2 = fields.rs2
        self.funct7 = fields.funct7


class IType(Format):
    def __init__(self, instruction: 'Instruction', config: Config) -> None:
        fields = _Fields(instruction, config)
        self.opcode7 = fields.opcode7
        self.opcode5 = fields.opcode5
        self.rd = fields.rd
        self.funct3 = fields.funct3
        self.rs1 = fields.rs1
        self.imm12 = fields.i_type_imm12
        self.imm = fields.i_type_imm


class SType(Format):
    def __init__(self, instruction: 'Instruction', config: Config) -> None:
        fields = _Fields(instruction, config)
        self.opcode7 = fields.opcode7
        self.opcode5 = fields.opcode5
        self.imm5 = fields.s_type_imm5
        self.funct3 = fields.funct3
        self.rs1 = fields.rs1
        self.rs2 = fields.rs2
        self.imm7 = fields.s_type_imm7
        self.imm = fields.s_type_imm


class BType(Format):
    def __init__(self, instruction: 'Instruction', config: Config) -> None:
        fields = _Fields(instruction, config)
        self.opcode7 = fields.opcode7
        self.opcode5 = fields.opcode5
        self.imm5 = fields.b_type_imm5
        self.funct3 = fields.funct3
        self.rs1 = fields.rs1
        self.rs2 = fields.rs2
        self.imm7 = fields.b_type_imm7
        self.imm = fields.b_type_imm


class UType(Format):
    def __init__(self, instruction: 'Instruction', config: Config) -> None:
        fields = _Fields(instruction, config)
        self.opcode7 = fields.opcode7
        self.opcode5 = fields.opcode5
        self.rd = fields.rd
        self.imm20 = fields.u_type_imm20
        self.imm = fields.u_type_imm
