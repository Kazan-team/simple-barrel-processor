# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information

# This is a WIP proposal for a new Pipeline Building Block API

from abc import ABCMeta, abstractmethod
from typing import Union, Tuple, Any, Optional, List, Iterable, Callable
from typing_extensions import final, Final
from nmigen import Signal, Module
from nmigen.hdl.ast import Assign
from nmigen.hdl.rec import Record, Layout
from nmigen.tools import bits_for

SignalShapeInput = Union['SignalShape',
                         Tuple[int, bool],
                         int]

ShapeInput = Union['Shape',
                   SignalShapeInput,
                   Layout,
                   List[Any]]


class Data(metaclass=ABCMeta):
    @abstractmethod
    def shape(self) -> 'Shape':
        ...

    @staticmethod
    @final
    def signals(data: 'DataType') -> Iterable[Signal]:
        """ returns all signals recursively in data
        """
        ...

    @abstractmethod
    def __getitem__(self, name: Any) -> 'DataType':
        ...

    @staticmethod
    @final
    def assign(lhs: 'DataType', rhs: 'DataType') -> List[Assign]:
        """ assigns all signals recursively in ``rhs`` to all the corresponding
            signals in ``lhs``.
            Assumes that ``lhs`` and ``rhs`` have the same ``Shape``.
        """
        shape = Shape.get(lhs)
        if shape != Shape.get(rhs):
            raise TypeError("shapes don't match")
        assignments = []
        for path in shape.signal_paths():
            lhs_signal = lhs
            rhs_signal = rhs
            for name in path:
                lhs_signal = lhs_signal[name]
                rhs_signal = rhs_signal[name]
            assert isinstance(lhs_signal, Signal)
            assert isinstance(rhs_signal, Signal)
            assignments.append(lhs_signal.eq(rhs_signal))
        return assignments


DataType = Union[Data,
                 Signal,
                 Record]


class Shape(metaclass=ABCMeta):
    @staticmethod
    @final
    def wrap(shape: ShapeInput) -> 'Shape':
        """ converts shape to an instance of Shape
        """
        ...

    @staticmethod
    @final
    def get(signal: DataType) -> 'Shape':
        """ gets the shape of signal
        """
        ...

    @final
    def __eq__(self, other: 'Shape') -> bool:
        """ structural equality
        """
        if isinstance(self, SignalShape):
            if isinstance(other, SignalShape):
                if self.nbits != other.nbits:
                    return False
                return self.signed == other.signed
            else:
                return False
        elif isinstance(other, SignalShape):
            return False
        return dict(self.members()) == dict(other.members())

    @final
    def signal_paths(self, path_prefix: Optional[List[str]] = None
                     ) -> Iterable[List[str]]:
        """ returns the paths to all signals recursively in self

            Example:

            ```
            paths = set(Shape.wrap(
                [
                    ("a", [
                        ("b", 1),
                        ("c", 2)
                    ]),
                    ("d", 1)
                ]))
            assert paths == set([["a", "b"], ["a", "c"], ["d"]])
            ```
        """
        path = [] if path_prefix is None else path_prefix
        if isinstance(self, SignalShape):
            yield path_prefix
        else:
            for name, member_shape in self.members():
                path.append(name)
                yield from member_shape.signal_paths(path_prefix)
                path.pop()

    @abstractmethod
    def create_data(self, name: Optional[str] = None) -> DataType:
        """ creates a DataType from self
        """
        ...

    @abstractmethod
    def members(self) -> Iterable[Tuple[str, 'Shape']]:
        """ gets the list of member attributes
        """
        ...


@final
class SignalShape(Shape):
    """ Shape for Signal
    """
    nbits: Final[int]
    signed: Final[bool]

    def create_data(self, name: Optional[str] = None) -> DataType:
        """ creates a Data signal from self
        """
        return Signal((self.nbits, self.signed), name=name)

    def members(self) -> Iterable[Tuple[str, 'Shape']]:
        return ()


@final
class RecordShape(Shape):
    """ Shape for Record
    """
    layout: Final[Layout]

    def create_data(self, name: Optional[str] = None) -> DataType:
        """ creates a Data signal from self
        """
        return Record(self.layout, name=name)

    def members(self) -> Iterable[Tuple[str, 'Shape']]:
        for name, (shape, direction) in self.layout.items():
            yield name, shape


@final
class Empty(Data):
    """ empty ``Data``
    """

    def shape(self) -> Shape:
        return EmptyShape()

    def __getitem__(self, name: Any) -> 'DataType':
        raise KeyError()


@final
class EmptyShape(Shape):
    """ empty ``Shape``
    """

    def create_data(self, name: Optional[str] = None) -> DataType:
        return Empty()

    def members(self) -> Iterable[Tuple[str, 'Shape']]:
        return ()


@final
class EntryPort:
    """ contains signals that handle data transfer into this block.

        `ready_out` is an output signal that is asserted by this block to
        indicate that this block is ready to accept a data transfer into this
        block. The data transfer occurs at the next clock edge if `valid_in` is
        also asserted.

        `valid_in` is an input signal that is set outside this block to
        indicate that there is data available to be transferred into this
        block. The data transfer occurs at the next clock edge if `ready_out`
        is also asserted.

        `valid_in` should not be combinationally dependent on `ready_out` to
        avoid combinational loops.

        The ready/valid signalling scheme is used partially because it supports
        several different schemes as special cases:
        * Global CE
            All blocks are either instances of `CombSeg`, `SimpleReg`, or
            `Chain`. The `CE` signal is sent in the top-level block's
            `exit.ready_in` and is connected directly through all blocks. The
            valid signals are ignored.

        * Traveling CE
            All blocks are either instances of `CombSeg`, `SimpleReg`, or
            `Chain`. The `CE` signal is sent in the top-level block's
            `entry.valid_in`. `exit.ready_in` is left set to its reset value
            of `1`.
    """

    data_in: Final[DataType]
    ready_out: Final[Signal]
    valid_in: Final[Signal]

    def __init__(self, shape: ShapeInput, name: str):
        self.data_in = Shape.wrap(shape).create_data(name + "_data_in")
        self.ready_out = Signal(1, name=name + "_ready_out")
        self.valid_in = Signal(1, reset=0, name=name + "_valid_in")

    def connect_from_entry(self, rhs: 'EntryPort') -> List[Assign]:
        return (Data.assign(self.data_in, rhs.data_in)
                + Data.assign(rhs.ready_out, self.ready_out)
                + Data.assign(self.valid_in, rhs.valid_in))

    def connect_from_exit(self, rhs: 'ExitPort') -> List[Assign]:
        return (Data.assign(self.data_in, rhs.data_out)
                + Data.assign(rhs.ready_in, self.ready_out)
                + Data.assign(self.valid_in, rhs.valid_out))


@final
class ExitPort:
    """ contains signals that handle data transfer out of this block.

        `ready_in` is an input signal that is set outside this block to
        indicate that the outside logic is ready to accept a data transfer out
        of this block. The data transfer occurs at the next clock edge if
        `valid_out` is also asserted.

        `valid_out` is an output signal that is asserted by this block to
        indicate that there is data available to be transferred out of this
        block. The data transfer occurs at the next clock edge if `ready_in` is
        also asserted.

        `valid_out` should not be combinationally dependent on `ready_in` to
        avoid combinational loops.

        See `EntryPort` for additional documentation
    """

    data_out: Final[DataType]
    ready_in: Final[Signal]
    valid_out: Final[Signal]

    def __init__(self, shape: ShapeInput, name: str):
        self.data_out = Shape.wrap(shape).create_data(name + "_data_out")
        self.ready_in = Signal(1, reset=1, name=name + "_ready_in")
        self.valid_out = Signal(1, name=name + "_valid_out")

    def connect_from_entry(self, rhs: EntryPort) -> List[Assign]:
        return (Data.assign(self.data_out, rhs.data_in)
                + Data.assign(rhs.ready_in, self.ready_out)
                + Data.assign(self.valid_out, rhs.valid_in))

    def connect_from_exit(self, rhs: 'ExitPort') -> List[Assign]:
        return (Data.assign(self.data_out, rhs.data_out)
                + Data.assign(rhs.ready_in, self.ready_in)
                + Data.assign(self.valid_out, rhs.valid_out))


class CombSeg(metaclass=ABCMeta):
    """ combinatorial segment of a side-effect-free data-path.
    can be used in code that takes a ``Block`` instance by passing to
    ``Block.wrap``.

    ``i`` is the input data
    ``o`` is the output data
    """

    def __init__(self, input_shape: Shape, output_shape: Shape):
        self.i = input_shape.create_data("i")
        self.o = output_shape.create_data("o")

    @abstractmethod
    def elaborate(self, platform: Any) -> Module:
        ...


@final
class CombSegFn:
    def __init__(self, input_shape: SignalShape, output_shape: Shape,
                 fn: Callable[[DataType, Module], DataType]):
        super().__init__(input_shape, output_shape)
        self.fn = fn

    def elaborate(self, platform: Any) -> Module:
        m = Module()
        Data.assign(self.o, self.fn(self.i, m))
        return m


@final
class CombChain(CombSeg):
    """ chain of CombSeg instances
    """

    def __init__(self, first_child: CombSeg, *children: CombSeg):
        children_tuple: Tuple[CombSeg, ...] = (first_child, *children)
        super().__init__(Shape.get(children_tuple[0].i),
                         Shape.get(children_tuple[-1].o))
        self.__children = children_tuple

    @property
    def children(self) -> Tuple[CombSeg, ...]:
        return self.__children

    def elaborate(self, platform: Any) -> Module:
        m = Module()
        last_output = self.i
        for index, child in enumerate(self.children):
            m.submodules.__setattr__(f"child_{index}", child)
            m.d.comb += Data.assign(child.i, last_output)
            last_output = child.o
        m.d.comb += Data.assign(self.o, last_output)
        return m


@final
class Identity(CombSeg):
    """ identity function
    """

    def __init__(self, shape: Shape):
        super().__init__(shape, shape)

    def elaborate(self, platform: Any) -> Module:
        m = Module()
        m.d.comb += Data.assign(self.o, self.i)
        return m


BlockLike = Union['Block', CombSeg]


class Block:
    """ building block for pipelines
    """

    entry: Final[EntryPort]
    exit: Final[ExitPort]

    def __init__(self, entry_shape: ShapeInput, exit_shape: ShapeInput):
        self.entry = EntryPort(entry_shape, "entry")
        self.exit = ExitPort(exit_shape, "exit")

    @staticmethod
    @final
    def wrap(block_like: BlockLike) -> 'Block':
        """ convert a ``BlockLike`` into a ``Block``
        """
        if isinstance(block_like, Block):
            return block_like
        if isinstance(block_like, CombSeg):
            return CombBlock(block_like)
        raise TypeError()

    @abstractmethod
    def elaborate(self, platform: Any) -> Module:
        ...


@final
class CombBlock(Block):
    """ class used for wrapping ``CombSeg``.
        should not be created directly, ``Block.wrap`` should be used instead.
    """

    def __init__(self, child: CombSeg):
        super().__init__(Shape.get(child.i), Shape.get(child.o))
        self.child = child

    def elaborate(self, platform: Any) -> Module:
        m = Module()
        m.submodules.child = self.child
        m.d.comb += Data.assign(self.child.i, self.entry.data_in)
        m.d.comb += Data.assign(self.exit.data_out, self.child.o)
        m.d.comb += Data.assign(self.entry.ready_out,
                                self.exit.ready_in)
        m.d.comb += Data.assign(self.exit.valid_out,
                                self.entry.valid_in)
        return m


@final
class Chain(Block):
    """ chain of ``BlockLike``.
    """

    def __init__(self, first_child: BlockLike, *children: BlockLike):
        children_tuple = tuple(
            (Block.wrap(child) for child in (first_child, *children)))
        super().__init__(Shape.get(children_tuple[0].entry.data_in),
                         Shape.get(children_tuple[-1].exit.data_out))
        self.__children = children_tuple

    @property
    def children(self) -> Tuple[Block, ...]:
        return self.__children

    def elaborate(self, platform: Any) -> Module:
        m = Module()
        last_child: Optional[Block] = None
        for index, child in enumerate(self.children):
            m.submodules.__setattr__(f"child_{index}", child)
            if last_child is None:
                m.d.comb += child.entry.connect_from_entry(self.entry)
            else:
                m.d.comb += child.entry.connect_from_exit(last_child.exit)
            last_child = child
        m.d.comb += self.exit.connect_from_exit(last_child.exit)
        return m


@final
class SimpleReg(Block):
    """ simple pipeline register.

        The `ready_*` signals are wired directly together.
    """

    def __init__(self, shape: ShapeInput):
        shape = Shape.wrap(shape)
        super().__init__(shape, shape)
        self.__data = shape.create_data("data")
        self.__valid = Signal(1, reset=0)

    def elaborate(self, platform: Any) -> Module:
        m = Module()
        m.d.comb += self.entry.ready_out.eq(self.exit.ready_in)
        m.d.comb += self.exit.data_out.eq(self.__data)
        m.d.comb += self.exit.valid_out.eq(self.__valid)
        with m.If(self.exit.ready_in):
            m.d.sync += self.__valid.eq(self.entry.valid_in)
            m.d.sync += self.__data.eq(self.entry.data_in)
        return m


@final
class Queue(Block):
    """ FIFO queue.

        `entries` is the number of entries in the FIFO. This must be >= 1.

        `allow_comb` means to allow transfers to transfer combinatorially from
        `entry` to `exit` in the same clock cycle. If `allow_comb` is `False`,
        then all output signals are produced only from internal registers or
        memory.

        `count` is the number of entries that are full.
    """

    entries: Final[int]
    allow_comb: Final[bool]
    count: Final[Signal]

    def __init__(self, shape: ShapeInput, entries: int, allow_comb: bool):
        shape = Shape.wrap(shape)
        super().__init__(shape, shape)
        if entries < 1:
            raise ValueError()
        self.entries = entries
        self.allow_comb = allow_comb
        self.count = Signal(bits_for(entries))

    def elaborate(self, platform: Any) -> Module:
        ...
