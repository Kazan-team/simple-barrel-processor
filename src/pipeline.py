# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information

from nmigen import Signal, Module, Mux, Record, Value, Array, Const
from nmigen.hdl.rec import Layout, Direction
from typing import (Union, Tuple, Optional, Any, Callable, List, ByteString,
                    Iterable)
from nmigen.tools import bits_for
from abc import ABCMeta, abstractmethod
from .util import (LayoutInput, layout_apply_default_direction,
                   layout_assert_direction, connect_records,
                   RecordConnectDirection, visit_records, SignalInputShape)


class StageToPred:
    """signals connected from the current pipeline stage to the
    preceding stage

    Parameters:
    -----------

    data_shape: Record layout

    Attributes:
    -----------
    data_in : Record(data_shape)
        Data
    valid_in : in
        Asserted by the preceding pipeline stage when it has data that will be
        ready to transfer by the next clock edge, including when ``ready_out``
        is deasserted.
    ready_out : out
        Asserted by this pipeline stage when it can accept data at the next
        clock edge, including when ``valid_in`` is deasserted.
    """

    def __init__(self,
                 data_shape: LayoutInput):
        data_shape = layout_apply_default_direction(Layout.wrap(data_shape),
                                                    Direction.FANIN)
        layout_assert_direction(data_shape, Direction.FANIN)
        self.data_in = Record(data_shape)
        self.valid_in = Signal(1)
        self.ready_out = Signal(1)

    def connect_between_stages(self, other: 'StageToSucc', m: Module) -> None:
        other.connect_between_stages(self, m)


class StageToSucc:
    """signals connected from the current pipeline stage to the
    succeeding stage

    Parameters:
    -----------

    data_shape: Record layout

    Attributes:
    -----------
    data_out : Record(data_shape)
        Data
    valid_out : out
        Asserted by this pipeline stage when it has data that will be ready to
        transfer by the next clock edge, including when ``ready_in`` is
        deasserted.
    ready_in : in
        Asserted by the succeeding pipeline stage when it can accept data at
        the next clock edge, including when ``valid_out`` is deasserted.
    """

    def __init__(self,
                 data_shape: LayoutInput):
        data_shape = layout_apply_default_direction(Layout.wrap(data_shape),
                                                    Direction.FANOUT)
        layout_assert_direction(data_shape, Direction.FANOUT)
        self.data_out = Record(data_shape)
        self.valid_out = Signal(1)
        self.ready_in = Signal(1)

    def connect_between_stages(self, other: StageToPred, m: Module) -> None:
        m.d.comb += self.ready_in.eq(other.ready_out)
        m.d.comb += other.valid_in.eq(self.valid_out)
        m.d.comb += connect_records(self.data_out, other.data_in,
                                    RecordConnectDirection.OutToIn)


class CombStage:
    """A pipeline stage containing combinational logic. Has a 0-cycle delay

    Parameters:
    -----------

    pred_layout: Record layout
        layout for ``pred.data_in``
    succ_layout: Record layout
        layout for ``succ.data_out``
    process: callable(pred_data: Record, succ_data: Record, m: Module)
        optional callable that defines the combinatorial logic. If ``process``
        is ``None``, then ``self.process`` should be overridden. If ``process``
        returns a ``Value`` then, ``succ.data_out.data`` is assigned using
         ``.eq`` to ``process``'s return value.

    Attributes:
    -----------
    pred : StageToPred
    succ : StageToSucc
    """

    def __init__(self,
                 pred_layout: LayoutInput,
                 succ_layout: LayoutInput,
                 process: Optional[Callable[[Record, Record, Module],
                                            Optional[Value]]] = None) -> None:
        self.pred = StageToPred(pred_layout)
        self.succ = StageToSucc(succ_layout)
        self.__process = process

    def process(self, m: Module) -> None:
        assert self.__process is not None
        v = self.__process(self.pred.data_in, self.succ.data_out, m)
        if v is not None:
            m.d.comb += self.succ.data_out.data.eq(v)

    def elaborate(self, platform: Any) -> Module:
        m = Module()
        m.d.comb += self.pred.ready_out.eq(self.succ.ready_in)
        m.d.comb += self.succ.valid_out.eq(self.pred.valid_in)
        self.process(m)
        return m


class RegStage:
    """A pipeline stage that delays by one clock cycle.

    Parameters:
    -----------

    layout : Record layout
        layout for ``pred.data`` and ``succ.data``

    Attributes:
    -----------
    pred : StageToPred
    succ : StageToSucc

    """

    def __init__(self,
                 layout: LayoutInput) -> None:
        self.pred = StageToPred(layout)
        self.succ = StageToSucc(layout)

    def elaborate(self, platform: Any) -> Module:
        m = Module()
        m.d.comb += self.pred.ready_out.eq(~self.succ.valid_out
                                           | self.succ.ready_in)
        m.d.sync += self.succ.valid_out.eq(self.pred.valid_in
                                           | (~self.succ.ready_in
                                              & self.succ.valid_out))

        def visitor(name: str,
                    pred: Tuple[Signal, Direction],
                    succ: Tuple[Signal, Direction]) -> None:
            m.d.sync += succ[0].eq(Mux(self.pred.valid_in
                                       & self.pred.ready_out,
                                       pred[0], succ[0]))

        visit_records(visitor, [self.pred.data_in, self.succ.data_out])
        return m


class BreakReadyChainStage:
    """A pipeline stage that delays by zero clock cycles and has
    ``pred.ready_out`` be the output of a flip-flop.

    Parameters:
    -----------

    layout : Record layout
        layout for ``pred.data`` and ``succ.data``

    Attributes:
    -----------
    pred : StageToPred
    succ : StageToSucc

    """

    def __init__(self,
                 layout: LayoutInput) -> None:
        layout = layout_apply_default_direction(Layout.wrap(layout),
                                                Direction.NONE)
        layout_assert_direction(layout, Direction.NONE)
        self.pred = StageToPred(layout)
        self.succ = StageToSucc(layout)
        layout = layout_apply_default_direction(layout,
                                                Direction.FANIN)
        self.buffer = Record(layout)
        self.buffer_full = Signal(1, reset=0)

    def elaborate(self, platform: Any) -> Module:
        m = Module()
        m.d.sync += self.buffer_full.eq(~self.succ.ready_in
                                        & (self.pred.valid_in
                                           | self.buffer_full))
        m.d.comb += self.succ.valid_out.eq(self.buffer_full
                                           | self.pred.valid_in)
        m.d.comb += self.pred.ready_out.eq(~self.buffer_full)

        def visitor(name: str,
                    pred_tuple: Tuple[Signal, Direction],
                    succ_tuple: Tuple[Signal, Direction],
                    buffer_tuple: Tuple[Signal, Direction]) -> None:
            pred = pred_tuple[0]
            succ = succ_tuple[0]
            buffer = buffer_tuple[0]
            m.d.comb += succ.eq(Mux(self.buffer_full, buffer, pred))
            m.d.sync += buffer.eq(succ)

        visit_records(visitor,
                      [self.pred.data_in,
                       self.succ.data_out,
                       self.buffer])
        return m


class TestSequence(metaclass=ABCMeta):
    def __init__(self, shape: SignalInputShape, reset: int = 0):
        self.pause = Signal(1, reset=0)
        self.output = Signal(shape, reset=reset)

    @abstractmethod
    def step(self, value: Value) -> Value:
        raise NotImplementedError()

    def elaborate(self, platform: Any) -> Module:
        m = Module()
        with m.If(self.pause):
            m.d.sync += self.output.eq(self.output)
        with m.Else():
            m.d.sync += self.output.eq(self.step(self.output))
        return m


class SuccessiveIntegerTestSequence(TestSequence):
    def step(self, value: Value) -> Value:
        return value + 1


_RC4KeyInitType = Union[None, int, List[int], str, ByteString]


class RC4:
    def __init__(self, key: _RC4KeyInitType = None):
        if key is None:
            key = [0]
        elif isinstance(key, int):
            key = key.to_bytes(max((key.bit_length() + 7) // 8, 1),
                               byteorder='little', signed=True)
        elif isinstance(key, str):
            key = bytes(key, 'utf-8')
        assert len(key) >= 1
        assert len(key) <= 1 << 8
        mem = [i for i in range(0, 1 << 8)]
        self.mem = mem
        j = 0
        for i in range(0, 1 << 8):
            j = (j + mem[i] + key[i % len(key)]) & 0xFF
            mem[i], mem[j] = mem[j], mem[i]
        self.i = 0
        self.j = 0
        for i in range(0, 3072):
            self.step()

    def step(self) -> int:
        return RC4StepState(self).apply(self)


class RC4StepState:
    def __init__(self, rc4: RC4):
        self.i = rc4.i
        self.j = rc4.j
        self.mem = list(rc4.mem)
        self.next_i = (self.i + 1) & 0xFF
        self.original_value_at_i = self.mem[self.next_i]
        self.next_j = (self.j + self.original_value_at_i) & 0xFF
        self.original_value_at_j = self.mem[self.next_j]
        self.new_value_at_i = self.original_value_at_j
        self.new_value_at_j = self.original_value_at_i
        self.k_addr = (self.new_value_at_i
                       + self.new_value_at_j) & 0xFF
        self.k_addr_is_next_i = self.k_addr == self.next_i
        self.k_addr_is_next_j = self.k_addr == self.next_j
        if self.k_addr_is_next_i:
            self.k = self.new_value_at_i
        if self.k_addr_is_next_j:
            self.k = self.new_value_at_j
        else:
            self.k = self.mem[self.k_addr]

    def apply(self, rc4: RC4) -> int:
        rc4.i = self.next_i
        rc4.j = self.next_j
        rc4.mem[self.next_i] = self.new_value_at_i
        rc4.mem[self.next_j] = self.new_value_at_j
        return self.k


class RandomTestSequence(TestSequence):
    """Generate a random test sequence

    Uses the RC4 algorithm (not for cryptography)
    """

    def __init__(self, width: int = 8,
                 init: Union[_RC4KeyInitType, RC4] = None):
        if width > 8:
            raise ValueError("width must not be greater than 8 bits")
        super().__init__(width, reset=1)
        if not isinstance(init, RC4):
            init = RC4(init)
        self.i = Signal(8, reset=init.i)
        self.j = Signal(8, reset=init.j)
        # using registers instead of memory to work around nmigen simulator bug
        # https://github.com/m-labs/nmigen/issues/47
        self.mem = [Signal(8,
                           name="mem_{0:03d}_{0:02X}".format(i),
                           reset=init.mem[i]) for i in range(1 << 8)]
        self.next_i = Signal(8)
        self.next_j = Signal(8)
        self.original_value_at_i = Signal(8)
        self.original_value_at_j = Signal(8)
        self.new_value_at_i = Signal(8)
        self.new_value_at_j = Signal(8)
        self.k_addr = Signal(8)
        self.k_addr_is_next_i = Signal(1)
        self.k_addr_is_next_j = Signal(1)
        self.k = Signal(8)

    step = None

    def elaborate(self, platform: Any) -> Module:
        m = Module()

        def read_mem(addr: Value) -> Value:
            return Array(self.mem)[addr]

        m.d.comb += self.next_i.eq(self.i + 1)
        m.d.sync += self.i.eq(Mux(self.pause, self.i, self.next_i))
        m.d.comb += self.original_value_at_i.eq(read_mem(self.next_i))
        m.d.comb += self.next_j.eq(self.j + self.original_value_at_i)
        m.d.sync += self.j.eq(Mux(self.pause, self.j, self.next_j))
        m.d.comb += self.original_value_at_j.eq(read_mem(self.next_j))
        m.d.comb += self.new_value_at_i.eq(self.original_value_at_j)
        m.d.comb += self.new_value_at_j.eq(self.original_value_at_i)
        m.d.sync += [self.mem[a].eq(
            Mux(self.pause,
                self.mem[a],
                Mux(self.next_i == a,
                    self.new_value_at_i,
                    Mux(self.next_j == a,
                        self.new_value_at_j,
                        self.mem[a])))) for a in range(1 << 8)]
        m.d.comb += self.k_addr.eq(self.new_value_at_i + self.new_value_at_j)
        m.d.comb += self.k_addr_is_next_i.eq(self.k_addr == self.next_i)
        m.d.comb += self.k_addr_is_next_j.eq(self.k_addr == self.next_j)
        m.d.comb += self.k.eq(Mux(self.k_addr_is_next_i,
                                  self.new_value_at_i,
                                  Mux(self.k_addr_is_next_j,
                                      self.new_value_at_j,
                                      read_mem(self.k_addr))))
        m.d.comb += self.output.eq(self.k.part(0, len(self.output)))
        return m


class PatternTestSequence(TestSequence):
    def __init__(self, width: int,
                 pattern: Iterable[Union[int, bool]]):
        super().__init__(width)
        self.pattern = Array([Const(i, width) for i in pattern])
        self.index = Signal(bits_for(len(self.pattern)), reset=0)

    step = None

    def elaborate(self, platform: Any) -> Module:
        m = Module()
        m.d.comb += self.output.eq(self.pattern[self.index])
        m.d.sync += self.index.eq(Mux(self.pause,
                                      self.index,
                                      Mux(self.index + 1 >= len(self.pattern),
                                          0,
                                          self.index + 1)))
        return m


class RandomPatternTestSequence(PatternTestSequence):
    def __init__(self, width: int = 8,
                 pattern: Union[_RC4KeyInitType, RC4] = None,
                 pattern_length: int = 1 << 10):
        if width > 8:
            raise ValueError("width must not be greater than 8 bits")
        if not isinstance(pattern, RC4):
            pattern = RC4(pattern)
        super().__init__(width, [pattern.step() for _ in range(pattern_length)])


class TestSequenceSourceStage:
    def __init__(self, test_sequence: Optional[TestSequence] = None,
                 valid_sequence: Optional[TestSequence] = None):
        if valid_sequence is None:
            valid_sequence = RandomPatternTestSequence(
                1, 'TestSequenceSourceStage')
        if test_sequence is None:
            test_sequence = RandomPatternTestSequence()
        assert len(valid_sequence.output) == 1
        self.test_sequence = test_sequence
        self.valid_sequence = valid_sequence
        self.succ = StageToSucc([('data', test_sequence.output.shape())])

    def elaborate(self, platform: Any) -> Module:
        m = Module()
        m.submodules.test_sequence = self.test_sequence
        m.submodules.valid_sequence = self.valid_sequence
        m.d.comb += self.succ.data_out.data.eq(self.test_sequence.output)
        m.d.comb += self.succ.valid_out.eq(self.valid_sequence.output)
        m.d.comb += self.valid_sequence.pause.eq(0)
        m.d.comb += self.test_sequence.pause.eq(
            ~(self.succ.valid_out & self.succ.ready_in))
        return m


class TestSequenceDestStage:
    def __init__(self, test_sequence: Optional[TestSequence] = None,
                 ready_sequence: Optional[TestSequence] = None):
        if ready_sequence is None:
            ready_sequence = RandomPatternTestSequence(1,
                                                       'TestSequenceDestStage')
        if test_sequence is None:
            test_sequence = RandomPatternTestSequence()
        assert ready_sequence.output.shape()[0] == 1
        self.test_sequence = test_sequence
        self.ready_sequence = ready_sequence
        self.pred = StageToPred([('data', test_sequence.output.shape())])
        self.any_failures = Signal(1, reset=0)
        self.current_failure = Signal(1)

    def elaborate(self, platform: Any) -> Module:
        m = Module()
        m.submodules.test_sequence = self.test_sequence
        m.submodules.ready_sequence = self.ready_sequence
        m.d.comb += self.current_failure.eq(
            (self.test_sequence.output != self.pred.data_in.data)
            & self.pred.ready_out & self.pred.valid_in)
        m.d.comb += self.pred.ready_out.eq(self.ready_sequence.output)
        m.d.comb += self.ready_sequence.pause.eq(0)
        m.d.comb += self.test_sequence.pause.eq(
            ~(self.pred.valid_in & self.pred.ready_out))
        m.d.sync += self.any_failures.eq(
            self.any_failures | self.current_failure)
        return m
