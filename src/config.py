#!/usr/bin/env python3
# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information


class Config:
    __slots__ = ["xlen", "ilen"]

    xlen: int
    ilen: int

    def __init__(self, xlen: int = 64, ilen: int = 32):
        self.xlen = xlen
        self.ilen = ilen
