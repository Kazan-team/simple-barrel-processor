#!/usr/bin/env python3
# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information
from nmigen import Signal, Module, C, Cat, Repl
from nmigen.cli import main
from nmigen.tools import log2_int
from .config import Config
from typing import ClassVar, Any


class ALUOperation:
    ADD_SUB: ClassVar[int] = 0x0
    SLL: ClassVar[int] = 0x1
    SLT: ClassVar[int] = 0x2
    SLTU: ClassVar[int] = 0x3
    XOR: ClassVar[int] = 0x4
    SRL_SRA: ClassVar[int] = 0x5
    OR: ClassVar[int] = 0x6
    AND: ClassVar[int] = 0x7


class SimpleALU:
    def __init__(self, config: Config) -> None:
        self._config = config
        self.src1 = Signal(config.xlen)
        self.src2 = Signal(config.xlen)
        self.output = Signal(config.xlen)
        self.operation = Signal(3)
        self.alt_function = Signal(1)

    def elaborate(self, platform: Any) -> Module:
        m = Module()
        less_than_src1 = Signal((self._config.xlen + 1, True),
                                name="less_than_src1")
        less_than_src2 = Signal((self._config.xlen + 1, True),
                                name="less_than_src2")
        sra_src1 = Signal((self._config.xlen + 1, True),
                          name="sra_src1")
        sra_src2 = Signal((log2_int(self._config.xlen), True),
                          name="sra_src2")
        m.d.comb += sra_src2.eq(self.src2.part(0, log2_int(self._config.xlen)))
        with m.Switch(self.operation):
            with m.Case(ALUOperation.ADD_SUB):
                m.d.comb += self.output.eq(
                    (Repl(self.alt_function,
                          self._config.xlen) ^ self.src2
                     ) + Cat(self.alt_function,
                             C(0, self._config.xlen - 1)) + self.src1)
            with m.Case(ALUOperation.SLL):
                m.d.comb += self.output.eq(
                    self.src1 << self.src2.part(0,
                                                log2_int(self._config.xlen)))
            with m.Case(ALUOperation.SLT):
                m.d.comb += less_than_src1.eq(
                    Cat(self.src1,
                        self.src1[self._config.xlen - 1]))
                m.d.comb += less_than_src2.eq(
                    Cat(self.src2,
                        self.src2[self._config.xlen - 1]))
                m.d.comb += self.output.eq(less_than_src1 < less_than_src2)
            with m.Case(ALUOperation.SLTU):
                m.d.comb += less_than_src1.eq(
                    Cat(self.src1,
                        C(0, 1)))
                m.d.comb += less_than_src2.eq(
                    Cat(self.src2,
                        C(0, 1)))
                m.d.comb += self.output.eq(less_than_src1 < less_than_src2)
            with m.Case(ALUOperation.XOR):
                m.d.comb += self.output.eq(self.src1 ^ self.src2)
            with m.Case(ALUOperation.SRL_SRA):
                with m.If(self.alt_function):
                    m.d.comb += sra_src1.eq(
                        Cat(self.src1,
                            self.src1[self._config.xlen - 1]))
                with m.Else():
                    m.d.comb += sra_src1.eq(
                        Cat(self.src1,
                            C(0, 1)))
                m.d.comb += self.output.eq(sra_src1 >> sra_src2)
            with m.Case(ALUOperation.OR):
                m.d.comb += self.output.eq(self.src1 | self.src2)
            with m.Case(ALUOperation.AND):
                m.d.comb += self.output.eq(self.src1 & self.src2)
        return m


if __name__ == "__main__":
    alu = SimpleALU(Config())
    main(alu, ports=[alu.src1, alu.src2, alu.output,
                     alu.operation, alu.alt_function])
