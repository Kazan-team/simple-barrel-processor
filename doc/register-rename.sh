#!/bin/bash
# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information

cat <<'EOF'
// void func(uint64_t *ptr, uint64_t size)
func:
    // ptr passed in a0
    // size passed in a1
    slli a1, a1, 3
    add a1, a0, a1 // create pointer to end of array
    li a3, 123

loop:
    ld a2, (a0)
    mul a2, a2, a3
    sd a2, (a0)
    addi a0, a0, 8
    bne a0, a1, loop
    ret

assuming all branches are correctly predicted, then the instructions can be run as follows:

EOF
next_instr=0
instr_str=()
instr_executed=()
instr_reads=()
instr_writes=()
next_retired=0

function issue() {
    local orig="$1" renamed_fmt="$2" writes="$3" reads="$4"
    local str
    if [[ "$writes" == "" ]]; then
        printf -v str "$renamed_fmt" $reads
    else
        printf -v str "$renamed_fmt" $writes $reads
    fi
    instr_str["$next_instr"]="$str"
    instr_reads["$next_instr"]="$reads"
    instr_writes["$next_instr"]="$writes"
    ((next_instr++))
    orig+='"                                  '
    printf 'issue #%02i: "%s renamed to "%s"\n' "$next_instr" "${orig:0:20}" "$str"
}

reg_ready=()
cycle=1

function execute() {
    local i new_ready="" ready
    echo
    echo "clock cycle $((cycle++)):"
    while ((next_retired < next_instr)); do
        if [[ "${instr_executed[next_retired]}" != 1 ]]; then
            break
        fi
        printf 'retire #%02i: %s\n' $((next_retired + 1)) "${instr_str[next_retired]}"
        ((next_retired++))
    done
    for i in "${!instr_str[@]}"; do
        if [[ "${instr_executed[i]}" == 1 ]]; then
            continue
        fi
        ready=1
        if [[ "${instr_reads[i]}" != "" ]]; then
            for r in ${instr_reads[i]}; do
                if [[ "${reg_ready[r]}" != 1 ]]; then
                    ready=0
                    break
                fi
            done
        fi
        if ((ready)); then
            printf 'execute #%02i: %s\n' $((i + 1)) "${instr_str[i]}"
            new_ready+=" ${instr_writes[i]}"
            instr_executed[i]=1
        fi
    done
    if [[ "$new_ready" != "" ]]; then
        for i in $new_ready; do
            reg_ready[$i]=1
        done
    fi
}

next_reg=0
((a0 = next_reg++))
reg_ready["$a0"]=1
((a1 = next_reg++))
reg_ready["$a1"]=1
printf 'starting with a0 renamed to r%02i and a1 renamed to r%02i:\n' $a0 $a1

execute

((next_a1=next_reg++))
issue 'slli a1, a1, 3' 'slli r%02i, r%02i, 3' "$next_a1" "$a1"
a1="$next_a1"

((next_a1=next_reg++))
issue 'add a1, a0, a1' 'add r%02i, r%02i, r%02i' "$next_a1" "$a0 $a1"
a1="$next_a1"

((a3=next_reg++))
issue 'li a3, 123' 'li r%02i, 123' "$a3" ""

for i in {1..6}; do
    ((a2=next_reg++))
    issue 'ld a2, (a0)' 'ld r%02i, (r%02i)' "$a2" "$a0"

    ((next_a2=next_reg++))
    issue 'mul a2, a2, a3' 'mul r%02i, r%02i, r%02i' "$next_a2" "$a2 $a3"
    a2="$next_a2"

    issue 'sd a2, (a0)' 'sd r%02i, (r%02i)' "" "$a2 $a0"

    ((next_a0=next_reg++))
    issue 'addi a0, a0, 8' 'addi r%02i, r%02i, 8' "$next_a0" "$a0"
    a0="$next_a0"

    issue 'bne a0, a1, loop' 'bne r%02i, r%02i, loop' "" "$a0 $a1"

    execute
done
issue 'ret' 'ret' "" ""
execute
execute
execute
