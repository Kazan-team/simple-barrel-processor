# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information

from nmigen import Module, Memory
from nmigen.back.pysim import Simulator, Delay, Tick
import unittest


class TestMemory(unittest.TestCase):
    @unittest.skip(
        "nmigen's simulator is known to fail test, so skip till fixed\n"
        "see https://github.com/m-labs/nmigen/issues/47")
    def test(self) -> None:
        class TestModule:
            def __init__(self):
                self.mem = Memory(1, 2, name="mem", init=[0, 0])
                self.mem_rd0 = self.mem.read_port(synchronous=False)
                self.mem_rd1 = self.mem.read_port(synchronous=False)
                self.mem_wr0 = self.mem.write_port(priority=0)
                self.mem_wr1 = self.mem.write_port(priority=1)

            def elaborate(self, platform):
                m = Module()
                m.submodules.mem_rd0 = self.mem_rd0
                m.submodules.mem_rd1 = self.mem_rd1
                m.submodules.mem_wr0 = self.mem_wr0
                m.submodules.mem_wr1 = self.mem_wr1
                m.d.comb += self.mem_rd0.addr.eq(0)
                m.d.comb += self.mem_rd1.addr.eq(1)
                m.d.comb += self.mem_wr0.addr.eq(0)
                m.d.comb += self.mem_wr0.data.eq(1)
                m.d.comb += self.mem_wr0.en.eq(1)
                m.d.comb += self.mem_wr1.addr.eq(1)
                m.d.comb += self.mem_wr1.data.eq(1)
                m.d.comb += self.mem_wr1.en.eq(1)
                return m

        module = TestModule()
        with Simulator(module,
                       vcd_file=open("test.vcd", "w"),
                       gtkw_file=open("test.gtkw", "w"),
                       traces=[module.mem_rd0.data,
                               module.mem_rd1.data]) as sim:
            sim.add_clock(1e-6, 0.25e-6)

            def async_process():
                yield Delay(1e-7)
                self.assertEqual((yield module.mem_rd0.data), 0)
                self.assertEqual((yield module.mem_rd1.data), 0)
                yield Tick()
                yield Delay(1e-7)
                self.assertEqual((yield module.mem_rd0.data), 1)
                self.assertEqual((yield module.mem_rd1.data), 1)

            sim.add_process(async_process)
            sim.run()
