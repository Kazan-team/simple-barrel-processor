# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information

from src.pipeline import (CombStage, RegStage, BreakReadyChainStage,
                          SuccessiveIntegerTestSequence, RandomTestSequence,
                          TestSequenceSourceStage, TestSequenceDestStage, RC4,
                          RC4StepState, RandomPatternTestSequence)
from nmigen import Signal, Module
from nmigen.back.pysim import Simulator, Delay, Tick, Passive
from nmigen.hdl.ast import Assign, Value
from typing import Any, Generator, List, Union, Optional
import unittest


def create_simulator(module: Any, traces: List[Signal]) -> Simulator:
    return Simulator(module,
                     vcd_file=open("test.vcd", "w"),
                     gtkw_file=open("test.gtkw", "w"),
                     traces=traces)


AsyncProcessCommand = Union[Delay, Tick, Passive, Assign, Value]
ProcessCommand = Optional[AsyncProcessCommand]
AsyncProcessGenerator = Generator[AsyncProcessCommand, Union[int, None], None]
ProcessGenerator = Generator[ProcessCommand, Union[int, None], None]


class TestSuccessiveIntegerTestSequence(unittest.TestCase):
    def test(self) -> None:
        module = SuccessiveIntegerTestSequence(8, reset=5)
        with create_simulator(module, [module.pause, module.output]) as sim:
            sim.add_clock(1e-6, 0.25e-6)

            def async_process() -> AsyncProcessGenerator:
                v = 5
                for i in [1, 0, 1, 1, 1, 1, 0, 0, 1, 1,
                          1, 0, 1, 1, 1, 1, 0, 1]:
                    yield module.pause.eq(i)
                    yield Tick()
                    self.assertEqual((yield module.output), v)
                    if i == 0:
                        v += 1

            sim.add_process(async_process)
            sim.run()


class TestRandomTestSequence(unittest.TestCase):
    test_vectors = [([1, 2, 3, 4, 5],
                     [0xEC, 0x0E, 0x11, 0xC4, 0x79, 0xDC, 0x32, 0x9D,
                      0xC8, 0xDA, 0x79, 0x68, 0xFE, 0x96, 0x56, 0x81]),
                    ([1, 2, 3, 4, 5, 6, 7],
                     [0xAD, 0x26, 0x58, 0x1C, 0x0C, 0x5B, 0xE4, 0x5F,
                      0x4C, 0xEA, 0x01, 0xDB, 0x2F, 0x38, 0x05, 0xD5]),
                    ]

    def test_rc4(self) -> None:
        for test_in, test_out in self.test_vectors:
            rc4 = RC4(test_in)
            out = [rc4.step() for _ in test_out]
            self.assertEqual(test_out, out, "RC4 test vector mismatch")

    def test(self) -> None:
        for test_in, test_out in self.test_vectors:
            module = RandomTestSequence(8, test_in)
            with create_simulator(module,
                                  [module.pause, module.i,
                                   module.j, module.next_i, module.next_j,
                                   module.original_value_at_i,
                                   module.original_value_at_j,
                                   module.new_value_at_i,
                                   module.new_value_at_j,
                                   module.k_addr, module.k]) as sim:
                sim.add_clock(1e-6, 0.25e-6)

                def async_process() -> AsyncProcessGenerator:
                    i = 0
                    rc4 = RC4(test_in)
                    for paused in [1, 0, 0, 1, 0, 0, 1, 0, 1, 1,
                                   0, 1, 1, 1, 1, 0, 0, 1, 0, 1,
                                   0, 0, 0, 1, 1, 1, 0, 1, 1, 0,
                                   1, 0, 1, 0, 1, 1, 1]:
                        yield module.pause.eq(paused)
                        yield Tick()
                        if paused != 0:
                            continue
                        step_state = RC4StepState(rc4)
                        self.assertEqual((yield module.next_i),
                                         step_state.next_i)
                        self.assertEqual((yield module.original_value_at_i),
                                         step_state.original_value_at_i)
                        self.assertEqual((yield module.next_j),
                                         step_state.next_j)
                        self.assertEqual((yield module.original_value_at_j),
                                         step_state.original_value_at_j)
                        self.assertEqual((yield module.new_value_at_i),
                                         step_state.new_value_at_i)
                        self.assertEqual((yield module.new_value_at_j),
                                         step_state.new_value_at_j)
                        self.assertEqual((yield module.k_addr),
                                         step_state.k_addr)
                        self.assertEqual((yield module.k_addr_is_next_i),
                                         step_state.k_addr_is_next_i)
                        self.assertEqual((yield module.k_addr_is_next_j),
                                         step_state.k_addr_is_next_j)
                        self.assertEqual((yield module.k),
                                         step_state.k)
                        self.assertEqual((yield module.output),
                                         test_out[i])
                        step_state.apply(rc4)
                        i += 1

                sim.add_process(async_process)
                sim.run()

    def test_random_pattern_test_sequence(self) -> None:
        for test_in, test_out in self.test_vectors:
            module = RandomPatternTestSequence(8, test_in)
            with create_simulator(module,
                                  [module.pause,
                                   module.index,
                                   module.output]) as sim:
                sim.add_clock(1e-6, 0.25e-6)

                def async_process() -> AsyncProcessGenerator:
                    i = 0
                    for paused in [1, 0, 0, 1, 0, 0, 1, 0, 1, 1,
                                   0, 1, 1, 1, 1, 0, 0, 1, 0, 1,
                                   0, 0, 0, 1, 1, 1, 0, 1, 1, 0,
                                   1, 0, 1, 0, 1, 1, 1]:
                        yield module.pause.eq(paused)
                        yield Tick()
                        if paused != 0:
                            continue
                        self.assertEqual((yield module.output),
                                         test_out[i])
                        i += 1

                sim.add_process(async_process)
                sim.run()


class TestTestSequenceStages(unittest.TestCase):
    def test(self):
        class TestPipeline:
            def __init__(self):
                self.src = TestSequenceSourceStage()
                self.dest = TestSequenceDestStage()

            def elaborate(self, platform: Any) -> Module:
                m = Module()
                m.submodules.src = self.src
                m.submodules.dest = self.dest
                self.src.succ.connect_between_stages(self.dest.pred, m)
                return m

        module = TestPipeline()
        with create_simulator(module,
                              [module.src.succ.data_out.data,
                               module.dest.test_sequence.output,
                               module.src.succ.ready_in,
                               module.src.succ.valid_out,
                               module.dest.current_failure,
                               module.dest.any_failures]) as sim:
            sim.add_clock(1e-6, 0.25e-6)

            def process() -> ProcessGenerator:
                for i in range(500):
                    yield
                    self.assertEqual((yield module.dest.current_failure), 0)

            sim.add_sync_process(process)
            sim.run()


class TestRegStage(unittest.TestCase):
    def test(self):
        class TestPipeline:
            def __init__(self):
                self.src = TestSequenceSourceStage()
                self.reg = RegStage([('data', 8)])
                self.dest = TestSequenceDestStage()

            def elaborate(self, platform: Any) -> Module:
                m = Module()
                m.submodules.src = self.src
                m.submodules.reg = self.reg
                m.submodules.dest = self.dest
                self.src.succ.connect_between_stages(self.reg.pred, m)
                self.reg.succ.connect_between_stages(self.dest.pred, m)
                return m

        module = TestPipeline()
        with create_simulator(module,
                              [module.reg.pred.data_in.data,
                               module.reg.pred.valid_in,
                               module.reg.pred.ready_out,
                               module.reg.succ.data_out.data,
                               module.reg.succ.valid_out,
                               module.reg.succ.ready_in,
                               module.dest.current_failure,
                               module.dest.any_failures]) as sim:
            sim.add_clock(1e-6, 0.25e-6)

            def process() -> ProcessGenerator:
                for i in range(500):
                    yield
                    self.assertEqual((yield module.dest.current_failure), 0)

            sim.add_sync_process(process)
            sim.run()


class TestBreakReadyChainStage(unittest.TestCase):
    def test(self):
        class TestPipeline:
            def __init__(self):
                self.src = TestSequenceSourceStage()
                self.brk = BreakReadyChainStage([('data', 8)])
                self.dest = TestSequenceDestStage()

            def elaborate(self, platform: Any) -> Module:
                m = Module()
                m.submodules.src = self.src
                m.submodules.brk = self.brk
                m.submodules.dest = self.dest
                self.src.succ.connect_between_stages(self.brk.pred, m)
                self.brk.succ.connect_between_stages(self.dest.pred, m)
                return m

        module = TestPipeline()
        with create_simulator(module,
                              [module.brk.pred.data_in.data,
                               module.brk.pred.valid_in,
                               module.brk.pred.ready_out,
                               module.brk.buffer.data,
                               module.brk.succ.data_out.data,
                               module.brk.succ.valid_out,
                               module.brk.succ.ready_in,
                               module.dest.current_failure,
                               module.dest.any_failures]) as sim:
            sim.add_clock(1e-6, 0.25e-6)

            def process() -> ProcessGenerator:
                for i in range(500):
                    yield
                    self.assertEqual((yield module.dest.current_failure), 0)

            sim.add_sync_process(process)
            sim.run()


class TestCombStage(unittest.TestCase):
    def test(self):
        class TestPipeline:
            def __init__(self):
                self.src = TestSequenceSourceStage(
                    SuccessiveIntegerTestSequence(8))
                self.comb_stage = CombStage(
                    [('data', 8)],
                    [('data', 8)],
                    lambda pred, succ, m: pred.data + 2)
                self.dest = TestSequenceDestStage(
                    SuccessiveIntegerTestSequence(8, reset=2))

            def elaborate(self, platform: Any) -> Module:
                m = Module()
                m.submodules.src = self.src
                m.submodules.comb_stage = self.comb_stage
                m.submodules.dest = self.dest
                self.src.succ.connect_between_stages(self.comb_stage.pred, m)
                self.comb_stage.succ.connect_between_stages(self.dest.pred, m)
                return m

        module = TestPipeline()
        with create_simulator(module,
                              [module.comb_stage.pred.data_in.data,
                               module.comb_stage.pred.valid_in,
                               module.comb_stage.pred.ready_out,
                               module.comb_stage.succ.data_out.data,
                               module.comb_stage.succ.valid_out,
                               module.comb_stage.succ.ready_in,
                               module.dest.current_failure,
                               module.dest.any_failures]) as sim:
            sim.add_clock(1e-6, 0.25e-6)

            def process() -> ProcessGenerator:
                for i in range(500):
                    yield
                    self.assertEqual((yield module.dest.current_failure), 0)

            sim.add_sync_process(process)
            sim.run()
