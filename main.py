#!/usr/bin/env python3
# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information
from nmigen.cli import main
from src.alu import SimpleALU
from src.config import Config


if __name__ == "__main__":
    config = Config(xlen=64, ilen=32)
    m = SimpleALU(config)
    main(m, ports=[m.src1, m.src2, m.output, m.operation, m.alt_function])
